'use strict';

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Automatically load required grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
  });

  // Configurable paths
  var config = {
    app: 'app',
    vendor: 'inc',
    server: '.tmp',
    dist: 'build'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Project settings
    config: config,

    clean: {
      server: {
        files: [{
          dot: true,
          src: [
            '<%= config.server %>/*',
            '!<%= config.server %>/.git*'
          ]
        }]
      },
      dist: {
        files: [{
          dot: true,
          src: [
            '<%= config.dist %>/*',
            '!<%= config.dist %>/.git*'
          ]
        }]
      }
    },

    wiredep: {
      app: {
        src: ['<%= config.app %>/jade/parts/usemin:js.html'],
        overrides: {
          "jquery": {
            "main": "dist/jquery.min.js"
          }
        },
        ignorePath: /^(\.\.\/)*\.\./
      }
    },
    jade: {
      compile: {
        options: {
          data: {
            debug: false,
          }
        },
        files: [{
          expand: true,
          cwd: '<%= config.app %>/jade',
          src: [
            '**/*.jade'
          ],
          dest: '<%= config.server %>',
          ext: '.html'
        }]
      }
    },

    coffee: {
      options: {
        sourceMap: true,
        sourceRoot: ''
      },
      server: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>',
          src: [
            'js/**/*.coffee',
          ],
          dest: '<%= config.server %>',
          ext: '.js'
        }]
      }
    },
    coffeelint: {
      app: ['<%= config.app %>/js/*.coffee']
    },
    jshint: {
      options: {
        jshintrc: '<%= config.app %>/.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        '<%= config.server %>/js/**/*.js',
        '!<%= config.server %>/js/built.js'
      ]
    },
    jscs: {
      src: [
        "<%= config.server %>/js/**/*.js",
        '!<%= config.server %>/js/built.js'
      ],
      options: {
        config: ".jscsrc",
        esnext: true, // If you use ES6 http://jscs.info/overview.html#esnext
        verbose: true, // If you need output with rule names http://jscs.info/overview.html#verbose
        fix: true, // Autofix code style violations when possible.
        requireCurlyBraces: [ "if" ]
      }
    },

    sass: {
      options: {
        sourceMap: true,
        sourceMapEmbed: true,
        sourceMapContents: true,
        includePaths: ['.']
      },
      server: {
        files: [{
          expand: true,
          cwd: '<%= config.app %>/sass/',
          src: ['*.{scss,sass}'],
          dest: '<%= config.server %>/css',
          ext: '.css',
          /*rename: function(dest, src) {
            return dest + src.replace(/\.css$/, ".scss");
          }*/
        }]
      }
    },
    csslint: {
      options: {
        csslintrc: '<%= config.app %>/.csslintrc'
      },
      strict: {
        options: {
          import: 2
        },
        src: [
          '<%= config.server %>/css/**/*.css',
          '!<%= config.server %>/css/built.css'
        ]
      }
    },

    copy: {
      server: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.app %>',
          dest: '<%= config.server %>',
          src: [
            'images/*',
            '!images/.*',
          ]
        }]
      },
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= config.server %>',
          dest: '<%= config.dist %>',
          src: [
            'images/*',
            '!images/.*',
            '*.html',
          ]
        },{
          expand: true,
          cwd: '<%= config.server %>/concat',
          dest: '<%= config.dist %>',
          src: ['**/*']
        }]
      }
    },

    // prep call for usemin (target all html files)
    useminPrepare: {
      html: ['<%= config.server %>/index.html'],
      options: {
        dest: '<%= config.dist %>/'
      }
    },
    // final call for usemin (target all html files)
    usemin: {
      options: {
        dirs: ['<%= config.dist %>']
      },
      html: ['<%= config.server %>/*.html']
    },

    browserSync: {
      options: {
        watchTask: true,
        notify: false,
        background: true
      },
      dev: {
        options: {
          files: [
            '<%= config.server %>/{css,js}/**/*.{css,js}',
          ],
          port: 9000,
          server: {
            baseDir: ['<%= config.server %>', config.app],
            routes: {
              '/bower_components': './inc'
            }
          }
        }
      },
      dist: {
        options: {
          files: [
            '<%= config.server %>/{css,js}/**/*.{css,js}',
          ],
          port: 9000,
          server: {
            baseDir: ['<%= config.dist %>', config.app],
            routes: {
              '/bower_components': './inc'
            }
          }
        }
      }
    },
    bsReload: {
      all: {
        reload: true
      }
    },
    watch: {
      options: {
        spawn: false // Important, don't remove this!
      },
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep', 'bsReload:all']
      },
      jade: {
        files: ['<%= config.app %>/jade/**/*.jade'],
        tasks: ['jade', 'bsReload:all']
      },
      coffee: {
        files: ['<%= config.app %>/js/**/*.coffee'],
        tasks: ['coffeelint', 'coffee', 'jshint', 'jscs', 'bsReload:all']
      },
      sass: {
        files: ['<%= config.app %>/sass/**/*.{sass,scss}'],
        tasks: ['sass', 'csslint', 'bsReload:all']
      }
    },

    'sftp-deploy': {
      build: {
        auth: {
          host: '192.168.0.111',
          authKey: 'vagrant-vbox'
        },
        src: 'build',
        dest: '/home/vagrant/test',
        serverSep: '/',
        concurrency: 4,
        progress: true
      }
    }

  });

  grunt.registerTask('serve', 'start the server and preview your app', function (target) {

    if (target === 'dist') {
      return grunt.task.run([
        'build',
        'browserSync:dist',
        'watch'
      ]);
    }
    else {
      return grunt.task.run([
        'build:dev',
        'browserSync:dev',
        'watch'
      ]);
    }

  });

  grunt.registerTask('build:dev', [
    'clean:server',
    'wiredep', 'jade',
    'coffeelint', 'coffee', 'jshint', 'jscs',
    'sass', 'csslint',
    'copy:server'
  ]);

  grunt.registerTask('build:dist', [
    'clean:dist',
    'useminPrepare',
    'concat',
    'usemin',
    'copy:dist'
  ]);

  grunt.registerTask('build', [
    'build:dev',
    'build:dist',
    'deploy'
  ]);

  grunt.registerTask('deploy', [
    'sftp-deploy'
  ]);

  grunt.registerTask('default', [
    'serve',
  ]);
};
